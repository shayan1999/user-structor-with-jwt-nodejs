const validator = require('./validator')
const { body } = require('express-validator');

class UserValidator extends validator{
    userInfo(){
        return [
            body('name', 'نام خود را وارد کنید').notEmpty(),
            body('lName', 'نام خانوادگی خود را وارد کنید').notEmpty(),
            body('email', 'ایمیل صحیح نمیباشد').isEmail(),
            body('password', 'رمز عبور را وارد کنید').isLength({min: 5})
        ]
    }
    editUser(){
        return[
            body('profileImage', 'عکس را وارد کنید.').not().isEmpty(),
            body('name', 'نام را وارد کنید.').not().isEmpty()
        ]
    }
}

module.exports = new UserValidator;