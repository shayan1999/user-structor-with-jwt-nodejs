const validator = require('./validator')
const { body } = require('express-validator');

class authValidator extends validator{
    register(){
        return[
            body('email', 'فرمت ایمیل صحیح نیست').isEmail(),
            body('password', 'پسورد حداقل باید ۵ حرف باشد').isLength({min:5}),
            body('name', 'این فیلد اجباریست').not().isEmpty(),
        ]
    }
    login(){

    }
}

module.exports = new authValidator;