const User = require('../models/user');
const { validationResult } = require('express-validator');
let controller = require('./controller')

class UserController extends controller{
    async getAllUsersWithView(req, res, next){
        try {
            let users = await User.find({});
            res.render('index', {users: users, message: req.flash('info')})
        }catch (e) {
            next(e);
        }
    }
    async getAllUsers(req, res) {
        let users = await User.find({});
        res.status(200).json({
            success: true,
            data: users
        });
    }
    async getOneUser (req, res, next){
        try {
            let user = await User.findOne({_id: req.params.id})
            if(!user){
                this.error('چنین کاربری یافت نشد', 404)
            }
            res.status(200).json({
                success: true,
                data: (user) ? user : 'not found'
            });
        }catch (e) {
            next(e);
        }
    }

    async createUser (req, res, next){
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                let myErrors = errors.array().map((e)=> e.msg )
                // return res.status(422).json({ error: errors.array() });
                this.error(myErrors, 402)
            }
            let newUser = new User({
                name: req.body.name,
                lName: req.body.lName,
                email: req.body.email,
                password: req.body.password,
                age: parseInt(req.body.age)
            })
            await newUser.save();
            //for json return
            // res.status(200).json({
            //     success: true,
            //     data: 'user created'
            // })
            //for change page
            req.flash('info', 'added!')
            res.redirect(`${config.BaseUrl}/user/listView`)
        }catch (e) {
            next(e);
        }
    }

    async editUser(req, res, next){
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                let myErrors = errors.array().map((e)=> e.msg )
                // return res.status(422).json({ error: errors.array() });
                this.error(myErrors, 402)
            }
            let data = {
                name: req.body.name
            };
            if(req.file){
                data.profileImage= req.file.path.replace(/\\/g, '/');
            }
            await User.updateOne({_id: req.user.id}, {$set : data});
            return  res.status(200).json({
                success: true,
                message: 'با موفقیت آپدیت شد',
                status: 200
            })
        }catch (e) {
            next(e);
        }
    }
    
    async deleteUser(req, res, next){
        try {
            await User.deleteOne({_id: req.params.id})
            req.flash('info', 'updated!')
            return (res.redirect(`${config.BaseUrl}/user/listView`))
        }catch (e) {
            next(e);
        }
    }

    async updateUser(req, res, next){
        try {
            await User.updateOne({_id: req.params.id}, {$set: req.body}).then(result=>{
                res.status(200).json({
                    success: true,
                    data: req.body,
                    message: 'آپدیت شد'
                })
            }).catch(error =>{
                res.status(404).json({
                    success: false,
                    message: 'این کاربر وجود ندارد'
                })
            })
        }catch (e) {
            next(e);
        }
    }
}

module.exports = new UserController;