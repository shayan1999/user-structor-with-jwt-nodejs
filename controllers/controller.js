const autoBind = require('auto-bind');

class Controller {
    constructor() {
        autoBind(this)
    }

    error(message, status= 500, messageType){
        let error = new Error((typeof message === 'object') ? 'بیش از یک خطا وجود دارد' : message )
        error.status = status;
        if(typeof message === 'object') error.data = message;
        throw error;
    }
}

module.exports = Controller;