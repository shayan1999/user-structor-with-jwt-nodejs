const { validationResult } = require('express-validator');
let controller = require('./controller');
const User = require('../models/user');
const RefreshToken = require('../models/refreshToken');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

class authController extends controller{

    async register(req, res, next){
        try {
            let existUser = await User.findOne({email: req.body.email});
            if(existUser) this.error('کاربر وجود دارد', 409);
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                let myErrors = errors.array().map((e)=> e.msg )
                this.error(myErrors, 402);
            }
            const hash = bcrypt.hashSync(req.body.password, 10);
            console.log(hash)
            let newUser= new User({
                name: req.body.name,
                email: req.body.email,
                password: hash
            });
            let accessToken = null;
            await newUser.save().then(async result =>{
                console.log(result);
                accessToken= this.generateAccessToken({email: req.body.email, id: result._id});
                const refreshToken= this.generateRefreshToken({email: req.body.email, id: result._id});
                const saveRefreshToken = new RefreshToken({refreshToken: refreshToken, userId: result._id});
                await saveRefreshToken.save();
                return res.status(201).json({
                    success: true,
                    status: 201,
                    message: 'با موفقیت ساخته شد',
                    accessToken,
                    refreshToken
                })
            })
        }catch (e) {
            next(e);
        }
    }

    async token(req, res, next){
        console.log('hey')
        const refreshToken= req.body.token;

        if(!refreshToken) return res.sendStatus(401);
        let findToken = await RefreshToken.findOne({refreshToken: refreshToken});

        if(!findToken) return res.sendStatus(401);
        jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user)=>{
            if(err) return res.sendStatus(403);
            const accessToken =this.generateAccessToken(user);
            res.json({
                accessToken: accessToken
            })
        })
        // await RefreshToken.deleteOne({_id: findToken._id});

    }

    async login(req, res, next){
        try {
            let user = await User.findOne({email: req.body.email});
            if(!user){
                this.error('این کاربر وجود ندارد', 404);
            }
            if(!bcrypt.compareSync(req.body.password, user.password)){
                this.error('رمز عبور صحیح نمیباشد', 401);
            }
            let newUser = {
                email: user.email,
                id: user._id
            }
            const accessToken= this.generateAccessToken(newUser);
            const refreshToken= this.generateRefreshToken(newUser);
            const saveRefreshToken = new RefreshToken({refreshToken: refreshToken, userId: user._id});
            await saveRefreshToken.save();
            res.status(200).json({
                accessToken: accessToken,
                refreshToken: refreshToken,
                message: 'وارد شدید',
                status: 200,
                success: true
            })
        }catch (e) {
            next(e);
        }
    }

    generateAccessToken(user){
        return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, {expiresIn: 60*60*24*30});
    }
    generateRefreshToken(user){
        return jwt.sign(user, process.env.REFRESH_TOKEN_SECRET);
    }
}

module.exports = new authController();