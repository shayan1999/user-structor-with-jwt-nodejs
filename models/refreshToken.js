const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = require('mongodb').ObjectID;

const refreshTokenSchema = new Schema({
    refreshToken:{
        type: String,
        required: true
    },
    userId:{
        type: ObjectId,
        required: true
    }
})

module.exports = mongoose.model('refreshToken', refreshTokenSchema, 'refreshToken');