const multer = require('multer');
const mkdir = require('mkdirp');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
      mkdir('./public/upload/userPics').then(made =>{
          cb(null, './public/upload/userPics')
      })
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname );
  }
})

const upload = multer({ storage: storage });

module.exports = upload;