const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken')
const RefreshToken = require('../models/refreshToken');

router.use(`${config.BaseUrl}/auth`, require('./auth'));
router.use((req, res, next) =>{
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if(!token) return res.sendStatus(401);
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user)=>{
        if(err) return res.sendStatus(403);
        req.user = user;
        next();
    })
})
router.post(`/api/node/logout`, async (req, res, next)=>{
    await RefreshToken.deleteMany({userId: req.user.id}).then(result =>{
        if(result.deletedCount !== 0){
            return res.status(200).json({
                success: true,
                message: 'خارج شدید'
            })
        }else{
            next(new Error('وجود ندارد'));
        }
    }).catch(err=>{
        next(err);
    })
})
router.use(`${config.BaseUrl}/user`, require('./user'));


router.all('*', async (req, res, next)=>{
    try {
        let error = new Error('صفحه‌ای یافت نشد')
        error.status = 404;
        throw error;
    }catch (e) {
        next(e)
    }
})

router.use(async (error, req, res, next)=>{
    const code = error.status || 500;
    const message = error.message || 'ارور سمت سرور';
    const stack = error.stack || '';
    const data = error.data || null

    if(config.debug){
        // return res.render('errors/developer', {message , stack})
        return res.status(code).json({
            message: message,
            status: code,
            stack:(code === 500) ? stack : null,
            data: (data) ? data : null,
            success: false
        })
    }else{
        return res.render(`errors/${code}`, {message})
    }
})

module.exports = router;