const express = require('express');
const router = express.Router();
const authValidator = require('../validators/authValidator')
const authController = require('../controllers/authController')

router.get('/logout', async (req, res)=>{
    console.log('hey')
})
// router.use((res, req, next)=>{
//     if (req.isAuthenticated()){
//         next();
//     }else{
//         return res.status(500).json({
//             message: 'شما یک اکانت دارید',
//             status: 500,
//             stack: null,
//             data: null,
//             success: false
//         })
//     }
// })
router.post('/login', authController.login);
router.post('/register', authValidator.register(), authController.register);
router.post('/token', authController.token);



module.exports = router;