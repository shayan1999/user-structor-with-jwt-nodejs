const express = require('express');
const router = express.Router();
const userValidator = require('../validators/userValidator')
const userController = require('../controllers/userController')
const uploadUserProfile = require('../upload/uploadUserProfile');
const jwt = require('jsonwebtoken');

//all users only data
router.get('/', userController.getAllUsers);
//all users with view
router.get('/listView', userController.getAllUsersWithView);
//make a new user
router.post('/', userValidator.userInfo() , userController.createUser);
//edit your details
router.put('/edit', uploadUserProfile.single('profileImage'), ((req, res, next)=>{
    if(!req.file){
        req.body['profileImage'] = null;
    }else{
        req.body['profileImage'] = req.file.filename;
    }
    next();
}), userValidator.editUser(), userController.editUser);
//see one user with id in params request
router.get('/:id', userController.getOneUser);
//update a user with id in params request
router.put('/:id', userValidator.userInfo(), userController.updateUser);
//delete a user with id in params request
router.delete('/:id', userController.deleteUser);

// for check smth and let go if we want
// router.use((req, res, next)=>{
//     if(parseInt(req.query.id) >= 3){
//         next();
//     }else{
//         return res.status(500).json({
//             success: false,
//             message: 'access denied'
//         })
//     }
// })

module.exports = router;