const express = require('express');
const app = express();
// const passport = require('passport');
const methodOverride = require('method-override')
const flash = require('connect-flash');
const cookieParser = require('cookie-parser')
const session = require('express-session')
const mongoose = require('mongoose');
// const MongoStore = require('connect-mongo')(session);
require('dotenv').config()
mongoose.connect('mongodb://localhost:27017/learning', {useNewUrlParser: true, useUnifiedTopology: true});


global.config = require('./config');

app.use(cookieParser(process.env.COOKIE_SECRET));
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: true,
    saveUninitialized: true,
}));
app.use(flash());
app.use(express.json());
// app.use(passport.initialize());
// app.use(passport.session());
app.use(methodOverride('method'));
app.use(express.static(__dirname + '/public'));
app.use(express.urlencoded({ extended : false}));
app.set('view engine', 'ejs');


app.use(`/`, require('./routes/index'))

app.listen(config.port, ()=>{
    console.log(`we are up now on localhost:${config.port} :D`)
})